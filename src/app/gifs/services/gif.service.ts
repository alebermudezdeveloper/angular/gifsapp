import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
// interfaces
import { GifInterface, SearchResponseInterface } from '../interfaces/gif.interfaces';

@Injectable({
  providedIn: 'root'
})
export class GifService {
  public gifList: GifInterface[] = [];
  private _tagsHistory: string[] = [];
  private apiKey: string = 'adkiTzVMdEAtJVsyHuMjv6RlXsWrmXPe';
  private serviceUrl: string = 'https://api.giphy.com/v1/gifs'

  constructor(
    private http: HttpClient
  ) { 
    this.getLocalStorage();
  }

  get tagsHistory() {
    return [...this._tagsHistory];
  }

  private getLocalStorage(): void {
    const storedData = localStorage.getItem('history');
    if (!storedData) return;
    this._tagsHistory = JSON.parse(storedData);
    if (this._tagsHistory.length === 0) return;
    this.searchTag(this._tagsHistory[0]);
  }

  private saveLocalStorage(): void {
    localStorage.setItem('history', JSON.stringify(this._tagsHistory));
  }

  private sortHistory(tag: string) {
    // lowercase
    tag = tag.toLowerCase();
    // check tag
    if (this._tagsHistory.includes(tag)) {
      // delete tag and return other tags
      this._tagsHistory = this._tagsHistory.filter((tagSearch: string) => tagSearch !== tag);
    }
    // add tag
    this._tagsHistory.unshift(tag);
    // limit to 10 items
    this._tagsHistory = this._tagsHistory.splice(0, 10);
    // save on storage
    this.saveLocalStorage();
  }

  async searchTag (tag: string): Promise<void> {
    // check lenght
    if (tag.length == 0) return;
    // sort history
    this.sortHistory(tag);
    // set params
    const params = new HttpParams()
      .set('api_key', this.apiKey)
      .set('limit', '10')
      .set('q', tag);
    // get data
    this.http.get<SearchResponseInterface>(`${ this.serviceUrl }/search`, { params }).subscribe( res => {
      // set data
      this.gifList = res.data;
    });
  }


}
