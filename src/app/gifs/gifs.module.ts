import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
// components
import { GifsHomeComponent } from './pages/gifs-home/gifs-home.component';
import { GifSearchComponent } from './components/gif-search/gif-search.component';
import { GifsListComponent } from './components/gifs-list/gifs-list.component';
import { GifCardComponent } from './components/gif-card/gif-card.component';


@NgModule({
  declarations: [  
    GifsHomeComponent, 
    GifSearchComponent, 
    GifsListComponent, GifCardComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [
    GifsHomeComponent
  ]
})
export class GifsModule { }
