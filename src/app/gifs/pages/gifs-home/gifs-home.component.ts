import { Component } from '@angular/core';
import { GifService } from '../../services/gif.service';
import { GifInterface } from '../../interfaces/gif.interfaces';

@Component({
  selector: 'gifs-home-page',
  templateUrl: './gifs-home.component.html',
  styleUrl: './gifs-home.component.scss'
})
export class GifsHomeComponent {
  // variables
  
  // constructor
  constructor(
    private gifSv: GifService
  ) {}
  // functions
  get gifs(): GifInterface[] {
    return this.gifSv.gifList;
  }
}
