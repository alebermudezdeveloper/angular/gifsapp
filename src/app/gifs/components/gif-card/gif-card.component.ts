import { Component, Input, OnInit } from '@angular/core';
import { GifInterface } from '../../interfaces/gif.interfaces';

@Component({
  selector: 'gif-card',
  templateUrl: './gif-card.component.html',
  styleUrl: './gif-card.component.scss'
})
export class GifCardComponent implements OnInit {
  // variables
  @Input()
  public gif!: GifInterface;
  // life page cycle
  ngOnInit(): void {
    if (!this.gif) throw new Error('Gif property is required');
  }
}
