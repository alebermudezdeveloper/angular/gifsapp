import { Component, ElementRef, ViewChild } from '@angular/core';
import { GifService } from '../../services/gif.service';

@Component({
  selector: 'gif-search-box',
  template: `
    <h5>Search:</h5>
    <input type="text" class="form-control" placeholder="Search Gifs..." (keyup.enter)="searchTag()" #txtTagInput>
  `,
  styleUrl: './gif-search.component.scss'
})
export class GifSearchComponent {
  // variables
  @ViewChild('txtTagInput')
  public tagInput!: ElementRef<HTMLInputElement>;
  // constructor
  constructor(
    private gifSv: GifService
  ) {}
  // functions
  searchTag (): void {
    // set tag
    const newTag = this.tagInput.nativeElement.value;
    // search tag
    this.gifSv.searchTag(newTag);
    // clear input
    this.tagInput.nativeElement.value = '';
  }
}
