import { Component, Input } from '@angular/core';
import { GifInterface } from '../../interfaces/gif.interfaces';

@Component({
  selector: 'gifs-list',
  templateUrl: './gifs-list.component.html',
  styleUrl: './gifs-list.component.scss'
})
export class GifsListComponent {
  @Input()
  public gifs: GifInterface[] = [];
}
