import { Component } from '@angular/core';
import { GifService } from '../../../gifs/services/gif.service';

@Component({
  selector: 'shared-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss'
})
export class SidebarComponent {
  // constructor
  constructor (
    private gifSv: GifService
  ) {}

  get tags(): string[] {
    return this.gifSv.tagsHistory;
  }

  searchTag(tag: string): void {
    this.gifSv.searchTag(tag);
  }

}
