import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'shared-lazy-image',
  templateUrl: './lazy-image.component.html',
  styleUrl: './lazy-image.component.scss'
})
export class LazyImageComponent implements OnInit {
  // variables
  @Input()
  public url!: string;
  @Input()
  public alt: string = '';
  public hasLoaded: boolean = false;
  // life page cycle
  ngOnInit(): void {
    if (!this.url) throw new Error('URL is required.');
  }

  onLoad(): void {
    this.hasLoaded = true;
  }
}
